
<h1 align="center">
  <br>
  <img src="img/icon.jpg" width="256px" alt="Sailbook">
  <br>
  <br>
  Sailbook
  <br>
  <br>
</h1>

Sailbook is an unofficial Facebook client for Ubuntu Touch OS based on Webkit

## Translations
POEditor [https://poeditor.com/join/project/LuP8Zh5Bsz](https://poeditor.com/join/project/LuP8Zh5Bsz)

## License

GNU General Public License v3.0
