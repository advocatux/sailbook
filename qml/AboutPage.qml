import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Dialog {
            id: aboutDialog
            visible: false
            title: i18n.tr("About Sailbook v2.0.13")
            text: i18n.tr("This is a unofficial Facebook Client for Ubuntu Touch.")

            Text {
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr('Copyright (c) 2017-2018 <br> by Rudi Timmermans  <br><br> E-Mail: <a href=\"mailto://rudi.timmer@mail.ch\">rudi.timmer@mail.ch</a>')
            }

            Text {
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr('Special thanks to Alain Molteni for the icon and splash screen.')
            }

            Text {
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr('Special thanks to testers, Tomas Öqvist, Sander Klootwijk, Joan CiberSheep, Mathijs Veen.')
            }

            Button {
                text: i18n.tr('Close')
                onClicked: PopupUtils.close(aboutDialog)
            }
        }
